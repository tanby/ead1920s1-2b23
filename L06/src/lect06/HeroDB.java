package lect06;

import java.sql.*;

public class HeroDB {
	public static Hero getHero(int id) {
		Hero h = new Hero();
		Connection conn = HeroDB.getConn();
		
		try {
			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery("SELECT * FROM hero WHERE idhero=" + id);
			if(rs.next()) {
				h.setName(rs.getString("name"));
				h.setIdhero(rs.getInt("idhero"));
				h.setPower(rs.getInt("power"));
			}
		}catch(Exception ex) {
			System.err.println(ex.getMessage());
		}
		
		
		return h;
	}
	// shortcut reusable method to get SQL Connection
	public static Connection getConn() {
		Connection conn = null;
		try {
			Class.forName("com.mysql.jdbc.Driver");
			conn = DriverManager.getConnection("jdbc:mysql://localhost/mydb?user=*****&password=*****");
		}catch(Exception ex) {
			System.err.println(ex.getMessage());
		}
		return conn;
	}
}
