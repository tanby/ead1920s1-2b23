<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page import="java.sql.*" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>CA2 pwd demo process</title>
</head>
<body>
<%
String uname = request.getParameter("uname");
String pwd = request.getParameter("pwd");
out.println(uname +","  + pwd);

//load driver
	Class.forName("com.mysql.jdbc.Driver");
	//out.println("driver loaded");
	
	// prepare URL
	// db = p0123456
	// u/n and pwd= p0123456
	String connURL ="jdbc:mysql://localhost/p0123456?user=p0123456&password=p0123456"; 
	
	// get connection
	Connection conn = DriverManager.getConnection(connURL);
	//out.println("conn started");
	
	// create statement
	PreparedStatement stmt = conn.prepareStatement("INSERT INTO member (name, password) VALUES(?,?)");
	
	// prepare sql and excute
	stmt.setString(1, uname);
	stmt.setString(2, pwd);
	   
	out.println(stmt);
 	int result = stmt.executeUpdate();
 	
 	// process the result
 	if(result>-1){
 		out.println("add success");
 	}else{
 		out.println("add failed!");
 	}
 	
 	conn.close();
%>
</body>
</html>