package ead; // it's like folders

public class Book {
	// properties or variables
	private String title = "EAD textbook";
	private int quantity = 1;
	// TODO: do the rest on your own. i ain't doing the work for you
	
	// methods
	// void = not return anything
	public void setTitle(String t) {
		title = t;
	}
	
	public String getTitle() {
		return title;
	}

	
	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}
	
	
}
