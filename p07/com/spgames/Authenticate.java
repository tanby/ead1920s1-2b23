package com.spgames;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Servlet implementation class Authenticate
 */
@WebServlet("/potato") // if you want another url for servlet
public class Authenticate extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Authenticate() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//doGet(request, response);
		// form is going to be submitted thru POST
		String potato = request.getParameter("username");
		
		// NEW: you have to create the out on your own in servlet
		PrintWriter out = response.getWriter();
		//out.println("Username: "+ potato);
		
		// If user keyed in the right credentials
		// DO the SQL part on your OWN!
		// DEMO: simple if check on username
		if(potato.equals("potato")) {
			// Create a Session attribute USER and 
			// store it with the entered loginid
			// NEW: need to create Session. different from jsp
			HttpSession session = request.getSession();
			session.setAttribute("USER", potato);
			
			// Redirect him/her to the another jsp
			response.sendRedirect("p07/restricted.jsp");
			
		}else {
			// kick user back to login
			response.sendRedirect("p07/login.html");
		}
	}

}
