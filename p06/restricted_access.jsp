<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>TBY Restricted Access page</title>
</head>
<body>
<h1>Restricted Access Page</h1>
<%
// Retrieve the session attribute LOGIN-STATUS and check:
//	If LOGIN-STATUS is null, redirect the user to login.jsp
// we have to CAST it. cos java not so smart need us to tell it
// so we have to put (String) to assure java
String checked = (String) session.getAttribute("LOGIN-STATUS");
if(checked == null){
	response.sendRedirect("login.jsp");
}
// REUSE this code. please DO NOT just copy and paste. 
// use include or class, etc. 
%>
</body>
</html>