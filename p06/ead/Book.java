package ead;

public class Book {
	// properties or fields
	// DEMO: only 2. the rest you do on your own
	private String title = "EAD how to serve coffee pages";
	private int quantity = 1;
	
	// methods
	public void setTitle(String t) {
		this.title = t;
	}
	public String getTitle() {
		return title;
	}
	public int getQuantity() {
		return quantity;
	}
	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}
	
}
