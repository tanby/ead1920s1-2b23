<%@page import="java.util.ArrayList, ead.*"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>TBY Shopping Cart</title>
</head>
<body>
<h1>Shopping Cart</h1>
<%
//	Retrieve the ArrayList that you stored in the session attribute book
ArrayList<Book> books = (ArrayList<Book>) session.getAttribute(
												"book");
//	Display the book object that you have stored in the ArrayList
for(int i=0; i<books.size(); i++){
	Book b = books.get(i);
	out.println("<div>" + b.getTitle() + ": " + 
			b.getQuantity() + "</div>");
}
%>
</body>
</html>