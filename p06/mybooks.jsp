<%@page import="java.util.ArrayList"%>
<%@page import="ead.Book"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>TBY ArrayList Demo</title>
</head>
<body>
<h1>ArrayList Demo</h1>
<%
/* •	Create a Value Bean class Book with the following attributes:
-	ISBN  (String)
-	Title (String)
-	Author (String)
-	Publisher (String)
-	Quantity (int)
-	Price (double)
view the video posted in term 1 in yammer
 */

//-	Create 3 Books objects
Book b1 = new Book();
// assume b1 use default 
Book b2 = new Book();
b2.setTitle("EHD how to hack");
b2.setQuantity(20);
Book b3 = new Book();
b3.setTitle("ACG how to hide");
b3.setQuantity(10);

// -	Store these objects inside a ArrayList
ArrayList<Book> books = new ArrayList<Book>();
books.add(b1);
books.add(b2);
books.add(b3);

// -	Retrieve all items stored in the ArrayList 
// and display them on the web-browser
for(int i=0; i<books.size(); i++){
	Book b = books.get(i);
	out.println("<div>" + b.getTitle() + ": " + 
			b.getQuantity() + "</div>");
}
 
// PART 3 partial - store this arraylist in Session
session.setAttribute("book", books);
%>
</body>
</html>