<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>TBY Logout</title>
</head>
<body>
<h1>Logout page</h1>
<%
// kill session. all session variables gone!!!
session.invalidate();
%>
</body>
</html>